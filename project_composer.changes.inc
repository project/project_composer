<?php

/**
 * Menu callback, list updates.
 *
 * Implementation of https://packagist.org/apidoc#track-package-updates.
 */
function project_composer_changes($release_category) {
  if ($release_category !== 'current') {
    return MENU_NOT_FOUND;
  }

  $time = (int) (microtime(TRUE) * 10000);

  // Get since parameter.
  if (isset($_REQUEST['since']) && ctype_digit($_REQUEST['since'])) {
    $since = (int) $_REQUEST['since'];
  }
  else {
    http_response_code(400);
    drupal_json_output([
      'error' => t('Invalid or missing "since" query parameter, make sure you store the timestamp at the initial point you started mirroring, then send that to begin receiving changes, e.g. !link for example.', [
        '!link' => url(variable_get('project_composer_packages_url', 'https://packages.drupal.org') . '/8/metadata/changes.json', ['query' => ['since' => $time], 'absolute' => TRUE]),
      ]),
      'timestamp' => $time,
    ]);
    return;
  }

  // Make sure time is not too far in the past.
  if ($time - $since > 24 * 60 * 60 * 10000) {
    drupal_json_output([
      'actions' => [[
        'type' => 'resync',
        'time' => floor($time / 10000),
        'package' => '*',
      ]],
      'timestamp' => $time,
    ]);
    return;
  }

  // Generate output.
  $actions = [];
  $result = db_query('SELECT package, updated, deleted FROM {project_composer_update_log} WHERE category = :release_category AND updated > :since', [
    ':release_category' => $release_category,
    ':since' => floor($since / 10000),
  ]);
  foreach ($result as $row) {
    $actions[] = [
      'type' => $row->deleted ? 'delete' : 'update',
      'package' => 'drupal/' . $row->package,
      'time' => $row->updated
    ];
  }

  drupal_json_output(['actions' => $actions, 'timestamp' => $time]);
}
