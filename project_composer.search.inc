<?php

define('PROJECT_COMPOSER_SEARCH_ROWS', 50);

/**
 * @param $release_category
 *   Either 'legacy' or 'current'.
 */
function project_composer_search($release_category) {
  drupal_page_is_cacheable(FALSE);

  $parameters = drupal_get_query_parameters();

  // 's' query parameter is required.
  if (!isset($parameters['s'])) {
    drupal_json_output(array(
      'error' => 'Missing search query, example: ?s=example',
    ));
    return;
  }

  // 'page' query parameter defaults to 1.
  if (isset($parameters['page'])) {
    $parameters['page'] = (int) $parameters['page'];
  }
  else {
    $parameters['page'] = 1;
  }

  try {
    $results = apachesolr_search_run('apachesolr', array(
      'fq' => array(
        'bundle:(project_module OR project_theme)',
        'sm_field_project_type:full',
        'sm_project_release_categories:' . $release_category,
      ),
      'fl' => array(
        'id',
        'entity_id',
        'entity_type',
        'bundle',
        'path',
        'is_uid',
        'ss_field_project_machine_name',
      ),
      'rows' => PROJECT_COMPOSER_SEARCH_ROWS,
      'spellcheck' => 'false',
      // Don't allow local params to pass through to EDismax from the url. We
      // also remove any remaining leading {! since that causes a parse error
      // in Solr.
      'q' => preg_replace('/^(?:{![^}]*}\s*)*(?:{!\s*)*/', ' ', $parameters['s']),
    ), '', '', $parameters['page'] - 1);
  }
  catch (Exception $e) {
    watchdog('project_composer', 'Search failed: ' . nl2br(check_plain($e->getMessage())), NULL, WATCHDOG_ERROR);
    drupal_json_output(array(
      'error' => 'Search failed!',
    ));
    return;
  }

  $output = array(
    'results' => array(),
    'total' => $GLOBALS['pager_total_items'][0],
  );

  // Results.
  foreach ($results as $result) {
    $output['results'][] = array(
      'name' => 'drupal/' . $result['fields']['ss_field_project_machine_name'],
      'description' => _project_composer_get_description($release_category, $result['fields']['entity_id'], $result['fields']['ss_field_project_machine_name']),
      'url' => $result['link'],
      'repository' => 'https://git.drupal.org/project/' . versioncontrol_project_repository_load($result['fields']['entity_id'])->name . '.git',
      'downloads' => 0,
      'favers' => 0,
    );
  }

  // Next.
  if (PROJECT_COMPOSER_SEARCH_ROWS * $parameters['page'] < $output['total']) {
    $output['next'] = url(variable_get('project_composer_packages_url', 'https://packages.drupal.org') . '/' . project_composer_core_version_string_from_release_category($release_category) . '/search.json', [
      'query' => [
        's' => $parameters['s'],
        'page' => $parameters['page'] + 1,
      ],
      'absolute' => TRUE,
    ]);
  }

  drupal_json_output($output);
}
