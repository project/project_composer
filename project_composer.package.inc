<?php

/**
 * @file
 * Provides storage functions for use during release packaging.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Clear the list of components and dependencies for a release.
 *
 * @param $release_nid
 *   The project release ID.
 */
function project_composer_info_package_clear($release_nid) {
  $component_ids = db_query(
    "SELECT component_id FROM {project_composer_component}
      WHERE release_nid = :release_nid",
    array(':release_nid' => $release_nid)
  )->fetchCol();

  if (!empty($component_ids)) {
    db_delete('project_composer_dependency')
      ->condition('component_id', $component_ids, 'IN')
      ->execute();
  }
  db_delete('project_composer_component')
    ->condition('release_nid', $release_nid)
    ->execute();
}

/**
 * Store the list of components for a release.
 *
 * @param $release_nid
 *   The project release ID.
 * @param $info
 *   Associative array of information for component keyed by the component name
 *   and providing the following keys: name, title, description.
 *
 * @return
 *   Associative array of stored records keyed by the component name with all
 *   the previous keys and the generated component_id.
 */
function project_composer_info_package_list_store($release_nid, array $info) {
  $records = array();
  foreach ($info as $component => $component_info) {
    $composer = (isset($component_info['composer'])) ? $component_info['composer'] : '';
    $package = (isset($component_info['package'])) ? $component_info['package'] : '';
    $core_version_requirement = (isset($component_info['core_version_requirement'])) ? $component_info['core_version_requirement'] : '';
    $core_api = (isset($component_info['core'])) ? $component_info['core'] : '';
    // These will always be insertions, because we've already deleted the old
    // ones.
    $project_composer_component_info = array(
      'release_nid' => $release_nid,
      'name' => $component_info['name'],
      'title' => $component_info['title'],
      'description' => $component_info['description'],
      'package' => $package,
      'composer' => $composer,
      'base_theme' => isset($component_info['base theme']) ? $component_info['base theme'] : '',
      'core_version_requirement' => $core_version_requirement,
      'core_api' => $core_api,
      'component_role' => $component_info['component_role'],
    );
    $result = drupal_write_record('project_composer_component', $project_composer_component_info);
    $records[$component] = $project_composer_component_info;
  }
  return $records;
}


/**
 * Store a dependency row into the project_composer_dependency table.
 *
 * @param $component_id
 *   The component_id for the component which depends on the dependencies.
 * @param array $dependencies
 *   Array of component_name => array('external' => TRUE|FALSE')
 * @param $dependency_type
 *   The type of the dependency (PROJECT_COMPOSER_DEPENDENCY_*)
 */
function project_composer_info_package_dependencies_store($component_id, array $dependencies, $dependency_type = PROJECT_COMPOSER_DEPENDENCY_REQUIRED) {
  foreach ($dependencies as $dependency => $attributes) {
    $record = array(
      'component_id' => $component_id,
      'dependency' => $dependency,
      'external' => !empty($attributes['external']),
      'dependency_category' => $attributes['dependency_category'],
      'dependency_source' => $attributes['dependency_source'],
      'dependency_type' => $dependency_type,
    );
    drupal_write_record('project_composer_dependency', $record);
  }
}
