<?php
/**
 * @file
 * Provide Drush integration for release building and dependency building.
 */

use Composer\Semver\VersionParser;

/**
 * Implements hook_drush_command().
 */
function project_composer_drush_command() {
  $items = array();

  $items['project-composer-process-project'] = array(
    'description' => 'Build composer metadata for a given project shortname.',
    'callback' => 'drush_project_composer_process_project',
    'arguments' => array(
      'shortname' => 'Project shortname/uri',
    ),
    'aliases' => array('pcpp'),
  );
  $items['project-composer-process-project-dependencies'] = array(
    'description' => 'Build dependencies for a given project shortname.',
    'callback' => 'drush_project_composer_process_project_dependencies',
    'arguments' => array(
      'shortname' => 'Project shortname/uri',
    ),
    'aliases' => array('pdpp'),
  );
  $items['project-composer-process-release'] = [
    'description' => 'Build dependencies for a given project version (primarily for debugging).',
    'arguments' => [
      'project' => 'Project machine name',
      'label' => 'VCS label like 7.x-3.x or 3.1.1',
    ],
  ];
  $items['project-composer-validate-constraints'] = array(
    'description' => 'Check the constraints to ensure that we only deliver valid ones',
    'callback' => 'drush_project_composer_validate_constraints',
    'aliases' => array('pcvc'),
  );
  $items['project-composer-delete-project-metadata'] = [
    'description' => 'Delete metadata for a project.',
    'arguments' => [
      'project' => 'Project machine name',
    ],
  ];

  return $items;
}


/**
 * Update and save all metadata for the project named.
 * accepts any number of project file names as arguments.
 */
function drush_project_composer_process_project() {
  $args = func_get_args();

  foreach ($args as $shortname) {
    drush_print("Processing package metadata for $shortname");
    // Get all versions for a project and build dependencies for them.
    $pid = project_get_nid_from_machinename($shortname);

    // Process the most recent release of a project for each release category.
    foreach (['legacy', 'current'] as $release_category) {
      $result = (new EntityFieldQuery())
        ->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', project_release_release_node_types())
        ->propertyCondition('status', NODE_PUBLISHED)
        ->fieldCondition('field_release_project', 'target_id', $pid)
        ->fieldCondition('field_release_category', 'value', $release_category)
        ->propertyOrderBy('nid', 'DESC')
        ->range(0, 1)
        ->execute();
      if (empty($result['node'])) {
        drush_print(dt('No @release_category releases for @shortname', [
          '@release_category' => $release_category,
          '@shortname' => $shortname,
        ]));
        continue;
      }
      $release_node = node_load(array_keys($result['node'])[0]);
      project_composer_process_release($release_node);
      $wrapper = entity_metadata_wrapper('node', $release_node);
      drush_print(dt('Processed @shortname: @version', [
        '@shortname' => $shortname,
        '@version' => $wrapper->field_release_vcs_label->value(),
      ]));
    }
  }
}

/**
 * Update and save all dependencies for the project named.
 *
 * @param $shortname
 *   Any number of project shortnames to be processed
 */
function drush_project_composer_process_project_dependencies() {
  module_load_include('inc', 'project_composer', 'project_composer.drupal');

  $args = func_get_args();

  foreach ($args as $shortname) {
    drush_print("Processing dependencies for $shortname");

    // Get all versions for a project and build dependencies for them.
    $pid = project_get_nid_from_machinename($shortname);

    // Find all releases for a project.
    $query = new EntityFieldQuery();
    $releases = $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'project_release')
      ->fieldCondition('field_release_project', 'target_id', $pid)
      ->propertyOrderBy('nid', 'DESC')
      ->execute();

    $release_nodes = array();
    if (!empty($releases['node'])) {
      $release_nodes = node_load_multiple(array_keys($releases['node']));
    }
    foreach ($release_nodes as $node) {
      $dependencies = project_composer_process_release_dependencies($node);

      $wrapper = entity_metadata_wrapper('node', $node);
      $version = $wrapper->field_release_vcs_label->value();
      if ($dependencies === FALSE) {
        drush_print("Skipped (or git failed) $shortname: {$version}");
        continue;
      }
      drush_print_r("Processed $shortname: {$version}");
    }
  }
}


/**
 * Process a single release node (version)
 * @param $shortname
 *   The project shortname (uri)
 * @param $version
 *   The version string (like 7.x-3.x-dev)
 */
function drush_project_composer_process_release($shortname, $version) {
  module_load_include('inc', 'project_composer', 'project_composer.drupal');

  $releases = (new EntityFieldQuery())->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'project_release')
    ->fieldCondition('field_release_project', 'target_id', project_get_nid_from_machinename($shortname))
    ->fieldCondition('field_release_vcs_label', 'value', $version)
    ->execute();
  if (empty($releases['node'] || count($release['node'] > 1))) {
    drush_print("Could not find $shortname: $version");
    return;
  }
  $node = node_load(array_keys($releases['node'])[0]);

  $dependencies = project_composer_process_release_dependencies($node);
  if ($dependencies === FALSE) {
    drush_print("Skipped $shortname: $version");
    return;
  }
  drush_print_r($dependencies);
}

/**
 * Runs all of the calculated dependency constraints
 */
function drush_project_composer_validate_constraints() {
  drush_print("Validating Constraints");
  $versionparser = new VersionParser();

  // Load the project.
  $constraints = db_query('SELECT DISTINCT dependency_constraint FROM {project_composer_calculated_dependency}')->fetchCol(0);

  foreach ($constraints as $constraint) {

    try {
      $versionparser->parseConstraints($constraint);
    }
    catch (Exception $e) {
      drush_print("Invalid Constraint: " . $e->getMessage());
    }
  }
}

/**
 * Delete metadata for a project.
 */
function drush_project_composer_delete_project_metadata($shortname) {
  if (!($project_node = project_load($shortname))) {
    drush_log(dt('Could not load project'), 'error');
  }

  // Get namespace map data.
  $result = db_query('SELECT map_id, component_name, package_namespace, category FROM {project_composer_namespace_map} WHERE project_nid = :project_nid', [
    ':project_nid' => $project_node->nid,
  ])->fetchAll(PDO::FETCH_ASSOC);
  $map_ids = array_column($result, 'map_id');
  if (!empty($map_ids)) {
    array_unshift($result, array_keys($result[0]));
    drush_print(drush_format_table($result));
  }

  // Get component data.
  if ($release_nids = _project_composer_get_release_nids($project_node->nid, ['legacy', 'current'])) {
    $result = db_query('SELECT component_id, component_role, name, package, release_nid, title FROM {project_composer_component} WHERE release_nid IN (:release_nids)', [
      ':release_nids' => $release_nids,
    ])->fetchAll(PDO::FETCH_ASSOC);
    $component_ids = array_column($result, 'component_id');
    if (!empty($component_ids)) {
      array_unshift($result, array_keys($result[0]));
      drush_print(drush_format_table($result));
    }
  }

  // Get files to delete.
  $filepaths = [];
  foreach (['legacy', 'current'] as $release_category) {
    if ($namespace = project_composer_get_project_namespace($project_node->nid, $release_category)) {
      foreach ([PROJECT_COMPOSER_METADATA_TAGGED, PROJECT_COMPOSER_METADATA_DEV] as $build_type) {
        $filepath = _project_composer_get_metadata_file_dir($release_category) . '/' . project_composer_get_filename($namespace, $build_type);
        if (file_exists($filepath)) {
          $filepaths[$release_category . '-' . $build_type] = $filepath;
        }
      }
    }
  }
  if (empty($filepaths)) {
    drush_print(dt('Nothing to delete.'));
    return;
  }

  // Confirm.
  drush_print(dt('You are about to delete:'));
  foreach ($filepaths as $filepath) {
    drush_print(dt('- @filepath', ['@filepath' => $filepath]));
  }
  if (!drush_confirm(dt('Do you want to continue?'))) {
    return;
  }

  // Delete from namespace map & components.
  if (!empty($map_ids)) {
    db_delete('project_composer_namespace_map')
      ->condition('map_id', $map_ids)
      ->execute();
  }
  if (!empty($component_ids)) {
    db_delete('project_composer_component')
      ->condition('component_id', $component_ids)
      ->execute();
  }

  // Delete files.
  foreach ($filepaths as $category_type => $filepath) {
    file_unmanaged_delete($filepath);
    project_composer_purge_packages_url(preg_replace('|public:/|', 'files', $filepath));

    // Update project_composer_update_log table.
    list($release_category, $build_type) = explode('-', $category_type);
    db_merge('project_composer_update_log')
      ->key([
        'category' => $release_category,
        'package' => $namespace . ($build_type === PROJECT_COMPOSER_METADATA_DEV ? '~dev' : ''),
      ])
      ->fields([
        'updated' => time(),
        'deleted' => 1,
      ])
      ->execute();
  }
}
