<?php

/**
 * Menu callback, list security advisories.
 */
function project_composer_security_advisories($release_category) {
  if ($release_category !== 'current') {
    return MENU_NOT_FOUND;
  }

  $project_nids = NULL;
  $updated_since = NULL;

  if (!empty($_REQUEST['packages']) && is_array($_REQUEST['packages'])) {
    // Get package namespaces.
    $names = [];
    foreach ($_REQUEST['packages'] as $package_name) {
      if (preg_match('#^drupal/(.+)$#', $package_name, $match)) {
        $names[] = $match[1];
      }
    }
    if (empty($names)) {
      drupal_json_output(['advisories' => []]);
      return;
    }

    // Get project node IDs.
    $project_nids = db_query("SELECT map.project_nid, map.map_id, min(dupe.map_id) FROM {project_composer_namespace_map} map INNER JOIN {project_composer_namespace_map} dupe ON dupe.category = map.category AND dupe.project_name = map.project_name WHERE map.category = 'current' AND map.package_namespace IN (:names) GROUP BY map.project_nid HAVING map.map_id = min(dupe.map_id) ORDER BY NULL", [
      ':names' => $names,
    ])->fetchCol();
    if (in_array('core', $names)) {
      $project_nids[] = 3060;
    }
    if (empty($project_nids)) {
      drupal_json_output(['advisories' => []]);
      return;
    }
  }
  elseif (isset($_REQUEST['updatedSince']) && ctype_digit($_REQUEST['updatedSince'])) {
    $updated_since = (int) $_REQUEST['updatedSince'];
  }
  else {
    http_response_code(400);
    drupal_json_output([
      'status' => 'error',
      'message' => t("Missing array of package names as the 'packages' parameter"),
    ]);
    return;
  }

  // Build output.
  $advisories = [];
  foreach (project_composer_get_security_advisory_json($project_nids, $updated_since) as $advisory) {
    $advisories[$advisory['packageName']][] = $advisory;
  }

  drupal_json_output(['advisories' => $advisories]);
}
