<?php

/**
 * @file
 * Provides Drupal implementation of project release info parsing and batch API.
 */

module_load_include('package.inc', 'project_composer');

/**
 * Get the info filename suffix expected for a release.
 *
 * @param EntityDrupalWrapper $release_wrapper
 *   The entity metadata wrapper for the release.
 *
 * @return string
 *   Either '.info.yml' or '.info'.
 */
function project_composer_info_file_suffix(EntityDrupalWrapper $release_wrapper) {
  static $info_file_patterns = [
    'current' => '.info.yml',
    'legacy' => '.info',
  ];

  return $info_file_patterns[$release_wrapper->field_release_category->value()];
}

/**
 * Parse all the .info files.
 *
 * @param $info_files
 *   List of component .info files.
 * @param EntityDrupalWrapper $release_wrapper
 *   The entity metadata wrapper for the release.
 *
 * @return
 *   Associative array of component information keyed by component name and
 *   merged with an array of defaults.
 */
function project_composer_info_parse(array $info_files, EntityDrupalWrapper $release_wrapper) {
  $defaults = [
    'name' => 'Unknown',
    'description' => '',
    'dependencies' => [],
    'test_dependencies' => [],
    'component_role' => 'subcomponent',
  ];

  $info = [];
  foreach ($info_files as $file) {
    $component = drupal_basename($file, project_composer_info_file_suffix($release_wrapper));
    if ($release_wrapper->field_release_category->value() === 'current') {
      // Drupal 8 & later use .info.yml files (except for releases before the
      // switch). We use Symfony module parsing since this modules is still D7
      // here. When porting this to D8, use native info file parser.
      try {
        $result = \Symfony\Component\Yaml\Yaml::parse(file_get_contents($file));
      }
      catch (\Symfony\Component\Yaml\Exception\ParseException $e) {
        watchdog('project_composer', 'Unable to parse the YAML string: %string', ['%string' => $e->getMessage()], WATCHDOG_ERROR);
      }
      if (is_null($result)) {
        $info[$component] = $defaults;
      }
      else {
        $info[$component] = $result + $defaults;
      }
    }
    else {
      // D7+ caches the result in the parsing of the file, so must be reset.
      drupal_static_reset('drupal_parse_info_file');
      $info[$component] = drupal_parse_info_file($file) + $defaults;
    }

    // Change info keys to suite project_dependency_info.
    $info[$component]['title'] = $info[$component]['name'];
    $info[$component]['name'] = $component;

    // Look for composer.json files associated with this component.
    $composer_json = dirname($file) . '/composer.json';
    if (file_exists($composer_json)) {
      $info[$component]['composer'] = file_get_contents($composer_json);
    }
  }
  return $info;
}

/**
 * Get the selected release node from the possible release nodes that contains
 * a component and is compatible with an API.  First look for a recommended
 * release.  If there is no recommended release then select the latest
 * release (highest nid).
 *
 * Sadly, the component may appear in more than one product, since there's no
 * guaranteed uniqueness of a submodule name. So we're just preferring the
 * most recent release node (highest nid).
 *
 * @param string $project_release_nids
 *   Possible project release nodes to consider.
 * @param string $parsed_component
 *   Parsed component array.
 * @param string $release_category
 *   Either 'legacy' or 'current'.
 *
 * @return object
 *   Release node object or FALSE if no release is found.
 */
function project_composer_get_release($project_release_nids, $parsed_component, $release_category) {
  // Select only supported releases of the right version.
  // Prefer recommended releases, highest major version and
  // most recent release (highest nid).
  $sql = db_select('project_release_supported_versions', 'supported');
  $sql->innerJoin('field_data_field_release_category', 'fdf_rc', 'fdf_rc.entity_id = supported.latest_release AND fdf_rc.field_release_category_value = :release_category', [':release_category' => $release_category]);
  $sql->fields('supported', ['recommended_release'])
    ->condition('supported.supported', 1)
    ->condition('supported.recommended_release', $project_release_nids, 'IN')
    ->orderBy('recommended', 'DESC')
    ->orderBy('branch', 'DESC')
    ->orderBy('recommended_release', 'DESC')
    ->orderBy('nid', 'DESC');
  if (isset($parsed_component['versions'])) {
    $items = $sql->execute();
    foreach ($items as $item) {
      $release = node_load($item->recommended_release);
      $version = project_composer_release_version($release);
      $result = drupal_check_incompatibility($parsed_component, $version);
      if (is_null($result)) {
        return $release;
      }
      else {
        $project_release_nids = array_diff($project_release_nids, [$item->recommended_release]);
      }
    }
  }
  else {
    $recommended = $sql->range(0, 1)->execute()->fetchField();
    if ($recommended) {
      return node_load($recommended);
    }
  }

  $query = (new EntityFieldQuery())->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'project_release')
    ->fieldCondition('field_release_category', 'value', $release_category)
    ->propertyCondition('nid', $project_release_nids)
    ->propertyOrderBy('nid', 'DESC');
  if (isset($parsed_component['versions'])) {
    $project_releases = $query->execute();
    if (!empty($project_releases['node'])) {
      foreach ($project_releases['node'] as $project_release) {
        $release = node_load($project_release->nid);
        $version = project_composer_release_version($release);
        $result = drupal_check_incompatibility($parsed_component, $version);
        if (is_null($result)) {
          return $release;
        }
      }
    }
  }
  else {
    $project_releases = $query->range(0, 1)->execute();
    if (!empty($project_releases['node'])) {
      $project_release = current($project_releases['node']);
      return node_load($project_release->nid);
    }
  }

  return FALSE;
}

/**
 * Return version without the Drupal version prefix.
 *
 * @param array $release
 *   Release node.
 *
 * @return string
 *   Return version
 */
function project_composer_release_version($release) {
  $wrapper = entity_metadata_wrapper('node', $release);
  $version = $wrapper->field_release_version->value();

  // Remove trailing -dev.
  if ($wrapper->field_release_build_type->value() === 'dynamic') {
    $version = preg_replace('/-dev$/', '', $version);
  }

  // Remove API compatibility component.
  if ($wrapper->field_release_project->type->value() !== 'project_core') {
    $api_vocabulary = taxonomy_vocabulary_load(variable_get('project_release_api_vocabulary', 0));
    if ($api_term = $wrapper->{'taxonomy_' . $api_vocabulary->machine_name}->value()) {
      $version = preg_replace('/^' . preg_quote($api_term->name, '/') . '-/', '', $version);
    }
  }

  return $version;
}

/**
 * Build dependency information for a single release node if it meets the
 * minimum Drupal major version API requirement.
 *
 * @param $node
 *   The project_release node for the release.
 *
 * @return
 *   An array of dependency information or FALSE on failure.
 */
function project_composer_process_release_dependencies(stdClass $node) {
  $wrapper = entity_metadata_wrapper('node', $node);

  if (!in_array($wrapper->field_release_category->value(), ['current', 'legacy'])) {
    return FALSE;
  }

  $release_project_name = $wrapper->field_release_project->field_project_machine_name->value();
  $project_nid = $wrapper->field_release_project->raw();
  $repository_url = versioncontrol_project_repository_load($project_nid)->remoteUrl();
  $label = $wrapper->field_release_vcs_label->value();
  watchdog('project_composer', 'Processing %project release from %repository_url, label %label', [
    '%project' => $release_project_name,
    '%repository_url' => $repository_url,
    '%label' => $label,
  ], WATCHDOG_INFO, l(t('view release'), 'node/' . $node->nid));
  $project_type = $wrapper->field_release_project->type->value();
  if (!in_array($project_type, ['project_core', 'project_module', 'project_theme'])) {
    return FALSE;
  }

  $work_tree = '/tmp/' . $release_project_name . '_' . time();
  $esc_work_tree = escapeshellarg($work_tree);
  $command = drush_shell_exec("git clone --depth 1 --branch %s %s %s", $label, $repository_url, $work_tree);
  if ($wrapper->field_release_build_type->value() === 'static') {
    // In case there is a branch with the same name as the tag, make sure we
    // have the tag.
    drush_shell_cd_and_exec($work_tree, 'git fetch --tags');
    drush_shell_cd_and_exec($work_tree, 'git checkout %s', 'refs/tags/' . $label);
  }

  // Scan checkout for .info files and create a list in the same format that
  // project release uses, so that standard API functions can be used.

  // Note that DRUPAL_PHP_FUNCTION_PATTERN croaks in file_scan_directory()
  // "WD php: Warning: ereg(): REG_ERANGE in file_scan_directory()"
  // $function_pattern = DRUPAL_PHP_FUNCTION_PATTERN;
  $function_pattern = '[a-zA-Z][a-zA-Z0-9_]*';
  $info_file_pattern = project_composer_info_file_suffix($wrapper);
  if ($project_type == 'project_core') {
    // Remove some modules that are for testing only.
    $options = ['nomask' => "/_test|test_|testing_/"];
  }
  else {
    $options = [];
  }
  $files = file_scan_directory($work_tree, '/^' . $function_pattern . preg_quote($info_file_pattern, '/') . '$/', $options);

  $info_files = [];
  foreach ($files as $file) {
    if (preg_match('/\/tests\/|\/Tests\//', $file->uri)) {
      // Remove modules in test directories.
      continue;
    }
    $info_files[] = $file->uri;
  }
  $info = project_composer_info_parse($info_files, $wrapper);
  // Some d7 modules made d8 'releases' that were nothing of the sort.
  if (empty($info)){
    $command = 'rm -rf ' . $esc_work_tree;
    exec($command, $output, $status);
    return FALSE;
  }

  // Determine the primary exention for this release
  // if the extension name == project machine name, that is first.
  // if not, we fall back to whatever component exists in the root of the project
  // finally, if no components exist in the root, we default to the shortest
  // component name assuming components like project, project_ext1, project_ext2
  // etc.


  $found_primary_component = FALSE;
  foreach ($info as $component => $component_info) {
    if ($component == $release_project_name && !$found_primary_component ) {
      $info[$component]['component_role'] = 'primary';
      $found_primary_component = TRUE;

    } else if (in_array("${work_tree}/${component}${info_file_pattern}", $info_files) && !$found_primary_component ) {
      $info[$component]['component_role'] = 'primary';
      $found_primary_component = TRUE;
    }
    foreach (['dependencies', 'test_dependencies'] as $dependency_type) {
      if (is_array($info[$component][$dependency_type])) {
        foreach ($info[$component][$dependency_type] as $key => $dependency) {
          $parsed_dependency = project_composer_parse_dependency_legacy($dependency);
          if (!isset($parsed_dependency['project'])) {
            // Dependency is not namespaced.
            if (isset($info[$parsed_dependency['name']])) {
              // Dependency is a component of the project itself.
              $info[$component][$dependency_type][$key] = $release_project_name . ':' . $dependency;
            }
            else {
              $project = project_composer_guess_project($parsed_dependency, $wrapper->field_release_category->value());
              if ($project) {
                $project_wrapper = entity_metadata_wrapper('node', $project);
                $info[$component][$dependency_type][$key] = $project_wrapper->field_project_machine_name->value() . ':' . $dependency;
              }
              else {
                // Probably should set an error message here.
                watchdog('project_composer', 'Failed to find a release for component %component as dependency of %depending_component, release_nid=%release_nid', [
                  '%component' => $parsed_dependency['name'],
                  '%depending_component' => $component,
                  '%release_nid' => $node->nid,
                ]);
              }
            }
          }
        }
      }
    }
  }
  if (!$found_primary_component) {
    // Otherwise, use the shortest component name. This will make ubercart use
    // uc_tax, but otherwise it’s probably close enough.
    $lengths = array_combine(array_keys($info), array_map('strlen', array_keys($info)));
    asort($lengths);
    $shortest = array_keys($lengths)[0];
    $info[$shortest]['component_role'] = 'primary';
  }

  $dependencies = [];
  foreach ($info as $module => $module_info) {
    $dependencies[$module] = $module_info['dependencies'];
    $test_dependencies[$module] = $module_info['test_dependencies'];
  }

  // Clear previous records for the release.
  project_composer_info_package_clear($node->nid);

  // Store the list of components contained by the project.
  $component_info = project_composer_info_package_list_store($node->nid, $info);
  foreach ($component_info as $component => $item) {
    // First process real, required dependencies.
    $new_dependencies = [];
    if (is_array($info[$component]['dependencies'])) {
      foreach ($info[$component]['dependencies'] as $new_dependency) {
        $category = project_composer_categorize_dependency($new_dependency, $release_project_name);

        $new_dependencies[$new_dependency] = ['external' => (strpos($new_dependency, $release_project_name . ':') !== 0), 'dependency_category' => $category, 'dependency_source' => 'info'];
      }
    }
    project_composer_info_package_dependencies_store($item['component_id'], $new_dependencies);

    // Now process test_dependencies as "RECOMMENDED".
    $test_dependencies = [];
    if (is_array($info[$component]['test_dependencies'])) {
      foreach ($info[$component]['test_dependencies'] as $new_dependency) {
        $category = project_composer_categorize_dependency($new_dependency, $release_project_name);
        $test_dependencies[$new_dependency] = ['external' => (strpos($new_dependency, $release_project_name . ':') !== 0), 'dependency_category' => $category, 'dependency_source' => 'info'];
      }
    }
    project_composer_info_package_dependencies_store($item['component_id'], $test_dependencies, PROJECT_COMPOSER_DEPENDENCY_RECOMMENDED);

    // Finally stash any dependencies we see from composer.
    if (isset($info[$component]['composer'])) {
      $composer_data = json_decode($info[$component]['composer']);
      // Loop through the requirements and store

      $requirekeys = ['require', 'require-dev'];
      foreach ($requirekeys as $key) {

        if (isset($composer_data->$key)) {
          $composer_dependencies = [];
          foreach ($composer_data->$key as $package_name => $package_version) {
            $dependency = "${package_name} (${package_version})";
            $category = project_composer_categorize_dependency($dependency);
            $composer_dependencies[$dependency] = ['dependency_category' => $category, 'dependency_source' => 'composer.json'];

          }
          if ($key == 'require') {
            $dependency_type = PROJECT_COMPOSER_DEPENDENCY_REQUIRED;
          }
          else {
            $dependency_type = PROJECT_COMPOSER_DEPENDENCY_RECOMMENDED;

          }
          project_composer_info_package_dependencies_store($item['component_id'], $composer_dependencies, $dependency_type);
        }
      }
    }
  }

  $command = 'rm -rf ' . $esc_work_tree;
  exec($command, $output, $status);

  // Cached releases on the project page have dependency information.
  project_release_download_table($project_nid, TRUE);

  return $dependencies;
}

/**
 * @param $new_dependency
 * @param $release_project_name
 *
 * @return string
 */
function project_composer_categorize_dependency($new_dependency, $release_project_name = '') {
  if (strpos($new_dependency, 'drupal/core') === 0) {
    $category = PROJECT_COMPOSER_CATEGORY_CORE;
  }
  else if (strpos($new_dependency, 'drupal/') === 0) {
      $category = PROJECT_COMPOSER_CATEGORY_CONTRIB;
  }
  else if (strpos($new_dependency, '/') !== FALSE) {
      $category = PROJECT_COMPOSER_CATEGORY_EXTERNAL;
  }
  else if (strpos($new_dependency, $release_project_name . ':') === 0) {
    $category = PROJECT_COMPOSER_CATEGORY_INTERNAL;
  }
  else if (strpos($new_dependency, 'drupal:') === 0) {
      $category = PROJECT_COMPOSER_CATEGORY_CORE;
  }
  else {
      $category = PROJECT_COMPOSER_CATEGORY_CONTRIB;
  }
  return $category;
}

/**
 * Parses a dependency for comparison by drupal_check_incompatibility().
 *
 * Clone of drupal_parse_dependency() with the project added in.  Needed until
 * drupal_parse dependency is fixed in Drupal 7 core.
 *
 * @param $dependency
 *   A dependency string, for example 'project:module (>=7.x-4.5-beta5, 3.x)'.
 *
 * @return
 *   An associative array with four keys:
 *   - 'project' includes the name of the project containing the module.
 *   - 'name' includes the name of the module to depend on.
 *   - 'original_version' contains the original version string (which can be
 *     used in the UI for reporting incompatibilities).
 *   - 'versions' is a list of associative arrays, each containing the keys
 *     'op' and 'version'. 'op' can be one of: '=', '==', '!=', '<>', '<',
 *     '<=', '>', or '>='. 'version' is one piece like '4.5-beta3'.
 *   Callers should pass this structure to drupal_check_incompatibility().
 *
 * @see drupal_check_incompatibility()
 */
function project_composer_parse_dependency_legacy($dependency) {
  $value = [];
  // Split out the optional project name.
  if (strpos($dependency, ':')) {
    list($project_name, $dependency) = explode(':', $dependency);
    $value['project'] = $project_name;
  }
  // We use named subpatterns and support every op that version_compare
  // supports. Also, op is optional and defaults to equals.
  $p_op = '(?P<operation>!=|==|=|<|<=|>|>=|<>)?';
  // Core version is always optional: 7.x-2.x and 2.x is treated the same.
  $p_core = '(?:' . preg_quote(DRUPAL_CORE_COMPATIBILITY) . '-)?';
  $p_major = '(?P<major>\d+)';
  // By setting the minor version to x, branches can be matched.
  $p_minor = '(?P<minor>(?:\d+|x)(?:-[A-Za-z]+\d+)?)';
  $parts = explode('(', $dependency, 2);
  $value['name'] = trim($parts[0]);
  if (isset($parts[1])) {
    $value['original_version'] = ' (' . $parts[1];
    foreach (explode(',', $parts[1]) as $version) {
      if (preg_match("/^\s*$p_op\s*$p_core$p_major\.$p_minor/", $version, $matches)) {
        $op = !empty($matches['operation']) ? $matches['operation'] : '=';
        if ($matches['minor'] == 'x') {
          // Drupal considers "2.x" to mean any version that begins with
          // "2" (e.g. 2.0, 2.9 are all "2.x"). PHP's version_compare(),
          // on the other hand, treats "x" as a string; so to
          // version_compare(), "2.x" is considered less than 2.0. This
          // means that >=2.x and <2.x are handled by version_compare()
          // as we need, but > and <= are not.
          if ($op == '>' || $op == '<=') {
            $matches['major']++;
          }
          // Equivalence can be checked by adding two restrictions.
          if ($op == '=' || $op == '==') {
            $value['versions'][] = ['op' => '<', 'version' => ($matches['major'] + 1) . '.x'];
            $op = '>=';
          }
        }
        $value['versions'][] = ['op' => $op, 'version' => $matches['major'] . '.' . $matches['minor']];
      }
    }
  }
  return $value;
}

/**
 * Guess the project for a dependency.
 *
 * Sadly, the component may appear in more than one project, since there's no
 * guaranteed uniqueness of a component name. Get the selected release node
 * that contains a component and is compatible with an API.  Restrict
 * possibilities to projects and component with the same name as the component
 * name and then consider all releases where the component name matches the
 * component name.
 *
 * @param string $parsed_component
 *   Parsed dependency array.
 * @param int $release_category
 *   Either 'legacy' or 'current'.
 *
 * @return object
 *   Project node object or FALSE if no release is found.
 */
function project_composer_guess_project($parsed_component, $release_category) {
  // Look for release with project name match, if available. Otherwise, look
  // for release with project name the same as the component name.
  $project_release_nids = db_query('SELECT DISTINCT release_nid FROM {project_composer_component} pcc
    LEFT JOIN {field_data_field_release_project} AS pr ON pcc.release_nid = pr.entity_id
    LEFT JOIN {field_data_field_project_machine_name} AS project ON pr.field_release_project_target_id = project.entity_id
    LEFT JOIN {node} AS release_node ON release_node.nid = pcc.release_nid
    WHERE pcc.name = :cname AND release_node.status = 1 AND project.field_project_machine_name_value = :pname', [
    ':cname' => $parsed_component['name'],
    ':pname' => isset($parsed_component['project']) ? $parsed_component['project'] : $parsed_component['name'],
  ])->fetchCol();
  if (!empty($project_release_nids)) {
    $release = project_composer_get_release($project_release_nids, $parsed_component, $release_category);
  }

  if (empty($release) && !isset($parsed_component['project'])) {
    // Look for a release from project that does not match the module name.
    $project_release_nids = db_query('SELECT DISTINCT release_nid FROM {project_composer_component} pcc
      LEFT JOIN {node} AS release_node ON release_node.nid = pcc.release_nid
      WHERE name = :cname AND release_node.status = 1', [':cname' => $parsed_component['name']])->fetchCol();
    if (empty($project_release_nids)) {
      return FALSE;
    }

    $release = project_composer_get_release($project_release_nids, $parsed_component, $release_category);
  }

  if (empty($release)) {
    return FALSE;
  }

  $wrapper = entity_metadata_wrapper('node', $release);
  return $wrapper->field_release_project->value();
}
